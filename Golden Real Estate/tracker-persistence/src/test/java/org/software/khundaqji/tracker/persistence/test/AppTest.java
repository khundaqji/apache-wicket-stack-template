package org.software.khundaqji.tracker.persistence.test;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import junit.framework.TestCase;

public class AppTest extends TestCase {

	public void testApp() {
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
//
//		Organization org = new Organization();
//		org.setName("GOLDEN REAL ESTATE");
//		org.setDescription("ALOT OF THINGS");
//
//		ProjectActivity pa1 = new ProjectActivity();
//		pa1.setName("Fire Extinguishers");
//		pa1.setProgress("LESSA");
//
//		ProjectActivity pa2 = new ProjectActivity();
//		pa2.setName("cleaning the facade");
//		pa2.setProgress("DONE");
//		
//		ProjectActivity pa3 = new ProjectActivity();
//		pa3.setName("Electr");
//		pa3.setProgress("DONE");
//		
//		List<ProjectActivity> projectActivites = new ArrayList<>();
//		projectActivites.add(pa1);
//		projectActivites.add(pa2);
//		projectActivites.add(pa3);
//		
//		Project p = new Project();
//		p.setBuildingName("FirstBuilding");
//		p.setActivities(projectActivites);
//		p.setProjectName("THE PROJECT NAME");
//		
//		List<Project> projects = new ArrayList<Project>();
//		projects.add(p);
//		org.setProjects(projects);
//		
//		Session session = sessionFactory.openSession();
//		System.out.println("Connection to Database is : " + session.isConnected());
//		
//		if(session.isOpen()){
//			Transaction transaction = session.beginTransaction();
//
//			session.save(org);
//			
//			transaction.commit();
//		}
		
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("FROM Project P WHERE P.organization.id = :organization");
		query.setParameter("organization", 1L);
		System.out.println(query.list().size());
		
		session.close();
	}
}