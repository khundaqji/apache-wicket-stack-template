package org.software.khundaqji.tracker.persistence.entity.common;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * @author Ahmad
 *
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

	/**
	 * Generated Serial Version of this class
	 */
	private static final long serialVersionUID = -7933078463838656603L;

	@Id
	@GenericGenerator(name = "IdGenerator", strategy = "org.software.khundaqji.project.domain.util.IdGenerator")
	@GeneratedValue(generator = "IdGenerator")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@PrePersist
	protected void prePersist() {
		// Step 1. Do what needed before persisting
	}
}
