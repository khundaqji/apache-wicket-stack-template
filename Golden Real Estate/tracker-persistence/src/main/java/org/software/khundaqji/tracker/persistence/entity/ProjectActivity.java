package org.software.khundaqji.tracker.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.software.khundaqji.tracker.persistence.entity.common.BaseEntity;

/**
 * 
 * @author Ahmad
 *
 */
@Entity
@Table(name = "PROJECT_ACTIVITY")
public class ProjectActivity extends BaseEntity {

	/**
	 * Generated Serial Version
	 */
	private static final long serialVersionUID = 6824771258585042965L;

	@Column(name = "name")
	private String name;

	// step 1. need to change this to ENUM
	@Column(name = "activity_progress")
	private int progress;

	@ManyToOne(fetch = FetchType.LAZY)
	private Project project;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}
