package org.software.khundaqji.tracker.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.software.khundaqji.tracker.persistence.entity.common.BaseEntity;

/**
 * 
 * @author Ahmad
 *
 */
@Entity
@Table(name = "SYSTEM_USER")
public class AppUser extends BaseEntity {

	/**
	 * Generated Serial
	 */
	private static final long serialVersionUID = 7404023391934192830L;

	public AppUser() {
	};

	public AppUser(String login) {
		this.login = login;

	};

	private String login;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
}