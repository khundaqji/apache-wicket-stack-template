package org.software.khundaqji.tracker.persistence.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.software.khundaqji.tracker.persistence.entity.common.BaseEntity;

/**
 * 
 * @author Ahmad
 *
 */
public abstract class BaseDAO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6790424275321748860L;

	protected SessionFactory sessionFactory;

	protected Session session;

	public BaseDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
		session = sessionFactory.openSession();
	}

	public abstract Object executeQuery(Query query);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<? extends BaseEntity> findAll(Class<? extends BaseEntity> entityClass) {

		Criteria criteria = session.createCriteria(entityClass);

		List list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public <T extends BaseEntity> T findOne(Class<T> entityClass, Serializable id) {
		Object entityObject = session.get(entityClass, id);
		if (entityObject instanceof BaseEntity) {
			return (T) entityObject;
		}
		return null;
	}

	/**
	 * 
	 * @param t
	 */
	public <T extends BaseEntity> T save(T entity) {
		session.persist(entity);
		return entity;
	}

	/**
	 * 
	 * This version of this method is working in a transactional mode
	 * 
	 * @param t
	 */
	public <T extends BaseEntity> T remove(T entity) {

		Transaction trx = session.beginTransaction();
		session.delete(entity);
		trx.commit();
		return entity;
	}

}
