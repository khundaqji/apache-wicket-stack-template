package org.software.khundaqji.tracker.persistence.dao.factory;

public enum DAOType {
	MYSQL, POSTGRESQL
}
