package org.software.khundaqji.tracker.persistence.dao.exception;

/**
 * 
 * @author akhundaqji
 *
 */
public class DAOCreationException extends Exception {

	private static final long serialVersionUID = -5576489728040960433L;

}
