package org.software.khundaqji.tracker.persistence.dao.factory;

import java.io.Serializable;

import org.hibernate.SessionFactory;
import org.software.khundaqji.tracker.persistence.dao.OrganizationDAO;
import org.software.khundaqji.tracker.persistence.dao.ProjectActivityDAO;
import org.software.khundaqji.tracker.persistence.dao.ProjectsDAO;
import org.software.khundaqji.tracker.persistence.dao.exception.DAOCreationException;
import org.software.khundaqji.tracker.persistence.dao.factory.impl.MySQLDAOFactory;

/**
 * 
 * @author akhundaqji
 * @version 1.0
 * @since 1.0
 *
 */
public abstract class DAOFactory implements Serializable {

	private static final long serialVersionUID = 5151146325306471576L;
	protected SessionFactory sessionFactory;

	public DAOFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public static DAOFactory createDAOFactory(SessionFactory sessionFactory, DAOType type) throws DAOCreationException {

		switch (type) {
		case MYSQL:
			return new MySQLDAOFactory(sessionFactory);
		case POSTGRESQL:
			break;
		}
		throw new DAOCreationException();
	}

	public OrganizationDAO createOrganizationDAO() {
		return new OrganizationDAO(this.sessionFactory);
	}

	public ProjectActivityDAO createProjectActivityDAO() {
		return new ProjectActivityDAO(this.sessionFactory);
	}

	public ProjectsDAO createProjectsDAO() {
		return new ProjectsDAO(this.sessionFactory);
	}
}
