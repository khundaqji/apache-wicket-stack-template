package org.software.khundaqji.tracker.persistence.dao.factory.impl;

import org.hibernate.SessionFactory;
import org.software.khundaqji.tracker.persistence.dao.OrganizationDAO;
import org.software.khundaqji.tracker.persistence.dao.factory.DAOFactory;

/**
 * 
 * @author akhundaqji
 * @version 1.0
 * @since 1.0
 *
 */
public class MySQLDAOFactory extends DAOFactory {

	private static final long serialVersionUID = 8811325493311208338L;

	public MySQLDAOFactory(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public OrganizationDAO createOrganizationDAO() {
		return new OrganizationDAO(sessionFactory);
	}

}
