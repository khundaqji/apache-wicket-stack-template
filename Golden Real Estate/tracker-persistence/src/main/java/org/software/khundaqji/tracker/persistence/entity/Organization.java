package org.software.khundaqji.tracker.persistence.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.software.khundaqji.tracker.persistence.entity.common.BaseEntity;

/**
 * 
 * @author Ahmad
 *
 */
@Entity
@Table(name = "ORGANIZATION")
public class Organization extends BaseEntity {

	/**
	 * Generated Serial Version
	 */
	private static final long serialVersionUID = -7464465204040573230L;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@OneToMany(mappedBy = "organization",  cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Project> projects;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
}
