package org.software.khundaqji.tracker.persistence.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

/**
 * 
 * @author Ahmad
 *
 */
public class ProjectsDAO extends BaseDAO {

	public ProjectsDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	private static final long serialVersionUID = 9166033299344453536L;

	public Object executeQuery(Long projectId) {
		Query query = this.session.createQuery("FROM ProjectActivity P WHERE P.project.id = :projectId");
		query.setParameter("projectId", projectId);
		return this.executeQuery(query);
	}

	@Override
	public Object executeQuery(Query query) {
		return query.list();
	}

}
