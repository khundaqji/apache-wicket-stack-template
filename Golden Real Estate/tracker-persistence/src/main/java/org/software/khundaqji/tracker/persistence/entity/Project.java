package org.software.khundaqji.tracker.persistence.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.software.khundaqji.tracker.persistence.entity.common.BaseEntity;

/**
 * 
 * @author Ahmad
 *
 */
@Entity
@Table(name = "PROJECT")
public class Project extends BaseEntity {

	/**
	 * Generated Serial ID
	 */
	private static final long serialVersionUID = 8952194649593272776L;

	@Column(name = "project_name")
	private String projectName;

	@Column(name = "building_name")
	private String buildingName;

	@OneToMany(mappedBy = "project", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ProjectActivity> activities;

	@ManyToOne(fetch = FetchType.LAZY)
	private Organization organization;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public List<ProjectActivity> getActivities() {
		return activities;
	}

	public void setActivities(List<ProjectActivity> activities) {
		this.activities = activities;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

}
