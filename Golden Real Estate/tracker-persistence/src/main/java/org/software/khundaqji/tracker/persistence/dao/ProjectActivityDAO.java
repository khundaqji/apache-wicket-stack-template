package org.software.khundaqji.tracker.persistence.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

import org.software.khundaqji.tracker.persistence.entity.ProjectActivity;

/**
 * 
 * @author ahmad
 *
 */
public class ProjectActivityDAO extends BaseDAO {

	private static final long serialVersionUID = -1360694378290448269L;

	public ProjectActivityDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	/**
	 * Create a new entity and saves it into the Database
	 * 
	 * @return
	 */
	public ProjectActivity createNewActivity(ProjectActivity activity) {
		return this.save(activity);
	}

	@Override
	public Object executeQuery(Query query) {
		return query.list();
	}

}
