package org.software.khundaqji.tracker.persistence.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;

/**
 * 
 * @author Ahmad
 *
 */
public class OrganizationDAO extends BaseDAO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8510497349713550086L;

	public OrganizationDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public Object executeQuery(Long organizationId) {
		Query query = this.session.createQuery("FROM Project P WHERE P.organization.id = :organizationId");
		query.setParameter("organizationId", organizationId);
		return this.executeQuery(query);
	}

	@Override
	public Object executeQuery(Query query) {
		return query.list();
	}

}
