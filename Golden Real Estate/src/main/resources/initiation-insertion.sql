create table ORGANIZATION (id  bigserial not null, description varchar(255), name varchar(255), primary key (id))
create table PROJECT (id  bigserial not null, building_name varchar(255), project_name varchar(255), organization_id int8, primary key (id))
create table PROJECT_ACTIVITY (id  bigserial not null, name varchar(255), activity_progress int8, project_id int8, primary key (id))
create table SYSTEM_USER (id  bigserial not null, login varchar(255), primary key (id))

alter table PROJECT add constraint FK_org_project foreign key (organization_id) references ORGANIZATION
alter table PROJECT_ACTIVITY add constraint FK_prjct_activity foreign key (project_id) references PROJECT

INSERT INTO ORGANIZATION VALUES (1, 'Golden Real Estate', 'Golden Real Estate Leading Porject Provider');

INSERT INTO PROJECT VALUES (1, 'Index Tower', 'Project number 43 - index tower', 1);

INSERT INTO PROJECT_ACTIVITY VALUES (1, 'renewing the fire extinguishers', 10, 1);
INSERT INTO PROJECT_ACTIVITY VALUES (2, 'cleaning the facade', 10, 1);
INSERT INTO PROJECT_ACTIVITY VALUES (3, 'fixing some defects', 20, 1);
INSERT INTO PROJECT_ACTIVITY VALUES (4, 'pulps maintainance', 5, 1);
INSERT INTO PROJECT_ACTIVITY VALUES (5, 'electric elevators', 90, 1);
INSERT INTO PROJECT_ACTIVITY VALUES (6, 'floors cleaning', 100, 1);
INSERT INTO PROJECT_ACTIVITY VALUES (7, 'windows', 30, 1);
INSERT INTO PROJECT_ACTIVITY VALUES (8, 'air conditioners', 10, 1);