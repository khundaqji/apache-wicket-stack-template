package org.software.khundaqji.project.domain.util;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * 
 * @author ahmad
 *
 */
public class IdGenerator implements IdentifierGenerator {

	@Override
	public Serializable generate(SessionImplementor session, Object obj) throws HibernateException {
		Serializable id = session.getEntityPersister(null, obj).getClassMetadata().getIdentifier(obj, session);
		return (id != null && (Long) id != 0) ? id : IdUtils.newId();
	}

}
