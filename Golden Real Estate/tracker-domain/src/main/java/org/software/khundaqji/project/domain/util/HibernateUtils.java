package org.software.khundaqji.project.domain.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * 
 * @author Ahmad
 * @version 1.1
 * @since 1.0
 *
 */
public class HibernateUtils {

	private static SessionFactory sessionFactory;

	static {
		try {
			sessionFactory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	public static final SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static final void shutdown() {
		getSessionFactory().close();
	}
}
