package org.software.khundaqji.project.domain.business;

import java.io.Serializable;
import java.lang.reflect.Constructor;

import org.hibernate.SessionFactory;
import org.software.khundaqji.project.domain.util.HibernateUtils;
import org.software.khundaqji.tracker.persistence.dao.BaseDAO;

/**
 * 
 * @author Ahmad
 *
 */
public abstract class SessionFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2486852799398928767L;

	protected SessionFactory sessionFactory;

	protected BaseDAO dao;

	/**
	 * 
	 */
	public SessionFacade() {
		this.sessionFactory = HibernateUtils.getSessionFactory();
	}

	/**
	 * 
	 * @param daoClass
	 * @return
	 * @throws Exception
	 */
	protected BaseDAO getDAO(Class<? extends BaseDAO> daoClass) throws Exception {

		BaseDAO dao = null;
		Class<?> cl = Class.forName(daoClass.getName());
		Constructor<?> cons = cl.getConstructor(SessionFactory.class);

		dao = (BaseDAO) cons.newInstance(this.sessionFactory);

		return dao;
	}
}
