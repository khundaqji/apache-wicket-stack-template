package org.software.khundaqji.project.domain.business;

import org.hibernate.Session;
import org.hibernate.Transaction;

import org.software.khundaqji.tracker.persistence.dao.ProjectActivityDAO;
import org.software.khundaqji.tracker.persistence.entity.Project;
import org.software.khundaqji.tracker.persistence.entity.ProjectActivity;

/**
 * 
 * @author ahmad
 *
 */
public class ProjectActivityFacade extends SessionFacade {

	private static final long serialVersionUID = 6765506151075172224L;

	public ProjectActivityFacade() {
		try {
			this.dao = getDAO(ProjectActivityDAO.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param activity
	 * @param projectId
	 * @return
	 */
	public ProjectActivity createNewActivity(ProjectActivity activity, Long projectId) {

		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Project project = (Project) session.get(Project.class, projectId);
		activity.setProject(project);
		session.persist(activity);
		transaction.commit();

		return activity;
	}

	/**
	 * 
	 * @param activityId
	 */
	public void deleteActivity(Long activityId) {

		ProjectActivity activity = this.dao.findOne(ProjectActivity.class, activityId);
		this.dao.remove(activity);

	}

	/**
	 * 
	 * @param activity
	 * @return
	 */
	public ProjectActivity updateActivity(ProjectActivity activity) {

		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		ProjectActivity activityUpdate = (ProjectActivity) session.get(ProjectActivity.class, activity.getId());
		activityUpdate.setName(activity.getName());
		activityUpdate.setProgress(activity.getProgress());
		session.persist(activityUpdate);
		transaction.commit();

		return activity;
	}

}
