package org.software.khundaqji.project.domain.business;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import org.software.khundaqji.tracker.persistence.dao.ProjectsDAO;
import org.software.khundaqji.tracker.persistence.entity.Organization;
import org.software.khundaqji.tracker.persistence.entity.Project;
import org.software.khundaqji.tracker.persistence.entity.ProjectActivity;
import org.software.khundaqji.tracker.persistence.entity.common.BaseEntity;

/**
 * 
 * @author ahmad
 *
 */
public class ProjectsFacade extends SessionFacade {

	private static final long serialVersionUID = 1196771630463628099L;

	private ProjectsDAO dao = new ProjectsDAO(sessionFactory);

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Project> findProjects() {
		List<? extends BaseEntity> orgs = dao.findAll(Project.class);
		return (List<Project>) orgs;
	}

	/**
	 * 
	 * @param projectId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ProjectActivity> findProjectActivities(Long projectId) {
		Object query = dao.executeQuery(projectId);
		return (List<ProjectActivity>) query;
	}

	/**
	 * 
	 * @param projectId
	 * @return
	 */
	public Project getProjectById(Long projectId) {
		return dao.findOne(Project.class, projectId);
	}

	/**
	 * 
	 * @param project
	 * @param orgId
	 * @return
	 */
	public Project createNewProject(Project project, Long orgId) {

		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Organization organization = (Organization) session.get(Organization.class, orgId);
		project.setOrganization(organization);
		session.persist(project);
		transaction.commit();

		return project;
	}

	/**
	 * 
	 * @param project
	 */
	public Project updateProject(Project project) {

		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Project projectUpdate = (Project) session.get(Project.class, project.getId());
		projectUpdate.setProjectName(project.getProjectName());
		projectUpdate.setBuildingName(project.getBuildingName());
		session.persist(projectUpdate);
		transaction.commit();

		return projectUpdate;
	}

	/**
	 * 
	 * @param projectId
	 */
	public void deleteProject(Long projectId) {
		Project project = this.dao.findOne(Project.class, projectId);
		this.dao.remove(project);

	}
}
