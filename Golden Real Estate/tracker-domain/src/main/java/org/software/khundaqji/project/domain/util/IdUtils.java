package org.software.khundaqji.project.domain.util;

import java.security.SecureRandom;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author ahmad
 *
 */
public class IdUtils {

	private static volatile SecureRandom numberGenerator = new SecureRandom();
	private static volatile short sequence = (short) numberGenerator.nextInt();

	public static synchronized long newId() {
		return (System.currentTimeMillis() << 20) | ((sequence++ & 0xFFFF) << 4) | (numberGenerator.nextInt() & 0xF);
	}

	public static String getHumanReadableId(long id) {
		int hash = (int) (id ^ (id >>> 32));
		hash = hash < 0 ? ~hash : hash;
		return StringUtils.upperCase(Integer.toString(hash, 32));
	}

	public static long random() {
		return numberGenerator.nextLong();
	}
}
