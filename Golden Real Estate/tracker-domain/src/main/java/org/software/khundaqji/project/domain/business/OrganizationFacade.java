package org.software.khundaqji.project.domain.business;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.software.khundaqji.tracker.persistence.dao.OrganizationDAO;
import org.software.khundaqji.tracker.persistence.dao.exception.DAOCreationException;
import org.software.khundaqji.tracker.persistence.dao.factory.DAOFactory;
import org.software.khundaqji.tracker.persistence.dao.factory.DAOType;
import org.software.khundaqji.tracker.persistence.entity.Organization;
import org.software.khundaqji.tracker.persistence.entity.Project;
import org.software.khundaqji.tracker.persistence.entity.common.BaseEntity;

/**
 * 
 * @author Ahmad
 *
 */
public class OrganizationFacade extends SessionFacade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3885292729733658904L;

	private DAOFactory daoFactory;

	private OrganizationDAO dao;

	public OrganizationFacade() {
		try {
			this.daoFactory = DAOFactory.createDAOFactory(this.sessionFactory, DAOType.MYSQL);
		} catch (DAOCreationException e) {
			e.printStackTrace();
		}
		this.dao = this.daoFactory.createOrganizationDAO();
	}

	/**
	 * Loads all the Organizations stored into the system
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Organization> findOrgainzations() {
		List<? extends BaseEntity> orgs = dao.findAll(Organization.class);
		return (List<Organization>) orgs;
	}

	/**
	 * 
	 * @param org
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Project> findOrgProjects(Long organizationId) {
		Object query = dao.executeQuery(organizationId);
		return (List<Project>) query;
	}

	/**
	 * 
	 * @param org
	 */
	public Organization createOrganization(Organization org) {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.persist(org);
		transaction.commit();
		return org;
	}

	/**
	 * 
	 * @param org
	 * @return
	 */
	public Organization updateOrganization(Organization org) {

		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Organization orgUpdate = (Organization) session.get(Organization.class, org.getId());
		orgUpdate.setName(org.getName());
		orgUpdate.setDescription(org.getDescription());
		session.persist(orgUpdate);
		transaction.commit();

		return orgUpdate;
	}

	/**
	 * 
	 * @param orgId
	 */
	public void deleteOrganization(Long orgId) {
		Organization org = this.dao.findOne(Organization.class, orgId);
		this.dao.remove(org);
	}
}
