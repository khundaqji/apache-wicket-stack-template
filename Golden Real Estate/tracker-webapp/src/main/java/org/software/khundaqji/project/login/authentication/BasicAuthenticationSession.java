package org.software.khundaqji.project.login.authentication;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.request.Request;

/**
 * 
 * @author Ahmad
 *
 */
public class BasicAuthenticationSession extends AuthenticatedWebSession {

	/**
	 * Generated Serial Version
	 */
	private static final long serialVersionUID = 1443945971769000241L;

	/*
	 * username field for holding this session
	 */
	private String username;

	/*
	 * 
	 */
	public BasicAuthenticationSession(Request request) {
		super(request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.wicket.authroles.authentication.AuthenticatedWebSession#
	 * authenticate(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean authenticate(String username, String password) {
		// user is authenticated if username and password are equal
		boolean authResult = username.equals(password);

		if (authResult)
			this.username = username;

		return authResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.wicket.authroles.authentication.
	 * AbstractAuthenticatedWebSession#getRoles()
	 */
	public Roles getRoles() {

		Roles resultRoles = new Roles();

		// if user is signed in add the relative role
		if (isSignedIn()) {
			resultRoles.add("SIGNED_IN");
		}

		// if username is equal to 'superuser' add the ADMIN role
		if (username != null && username.equals("superuser")) {
			resultRoles.add(Roles.ADMIN);
		}

		return resultRoles;
	}

	/**
	 * 
	 */
	@Override
	public void signOut() {
		super.signOut();
		username = null;
	}
}