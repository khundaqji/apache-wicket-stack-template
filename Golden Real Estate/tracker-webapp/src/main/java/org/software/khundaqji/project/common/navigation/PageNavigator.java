package org.software.khundaqji.project.common.navigation;

import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.markup.html.navigation.paging.IPagingLabelProvider;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;

/**
 * 
 * @author Ahmad
 *
 */
public class PageNavigator extends PagingNavigator {

	private static final long serialVersionUID = -3799552492378273313L;

	public PageNavigator(String id, IPageable pageable) {
		super(id, pageable);
	}

	public PageNavigator(String id, IPageable pageable, IPagingLabelProvider labelProvider) {
		super(id, pageable, labelProvider);
	}

}