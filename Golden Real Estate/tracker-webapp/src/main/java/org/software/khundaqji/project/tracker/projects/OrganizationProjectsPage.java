package org.software.khundaqji.project.tracker.projects;

import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.string.StringValue;
import org.apache.wicket.util.value.ValueMap;
import org.software.khundaqji.project.common.AbstractWebPage;
import org.software.khundaqji.project.common.navigation.PageNavigator;
import org.software.khundaqji.project.tracker.activities.ProjectActivitiesPage;
import org.software.khundaqji.tracker.persistence.entity.Project;

/**
 * 
 * @author Ahmad
 *
 */
@AuthorizeInstantiation("SIGNED_IN")
public class OrganizationProjectsPage extends AbstractWebPage {

	/**
	 * Generated Seial Version
	 */
	private static final long serialVersionUID = -6265568444709632670L;

	/**
	 * Creates a new OrganizationProjectsPage
	 * 
	 * @param parameters
	 */
	public OrganizationProjectsPage(final PageParameters parameters) {

		super(parameters);

		StringValue orgId = parameters.get("orgid");
		this.setupProjectTable(orgId.toString());
	}

	/**
	 * 
	 * @param org
	 */
	private void setupProjectTable(String org) {

		List<Project> projects = orgFacade.findOrgProjects(new Long(org));
		ListDataProvider<Project> projectDataProvider = new ListDataProvider<Project>(projects);
		DataView<Project> projectDataView = new DataView<Project>("projects", projectDataProvider) {

			private static final long serialVersionUID = 2318937182208288483L;

			@Override
			protected void populateItem(Item<Project> item) {
				Project proj = (Project) item.getModelObject();
				item.add(new Label("projectId", proj.getId()));
				item.add(new Label("projectName", proj.getProjectName()));
				item.add(new Label("buildingName", proj.getBuildingName()));

				final Button deleteButton = new Button("deleteBtn");
				deleteButton.add(AttributeModifier.replace("id", proj.getId()));
				item.add(deleteButton);

				final Button updateButton = new Button("updateBtn");
				updateButton.add(AttributeModifier.replace("id", proj.getId()));
				item.add(updateButton);

				item.add(new Link<Void>("projectActivities") {

					private static final long serialVersionUID = -71894828246953317L;

					@Override
					public void onClick() {
						PageParameters params = new PageParameters();
						params.add("projectId", proj.getId());
						setResponsePage(ProjectActivitiesPage.class, params);
					}

				});

			}
		};
		projectDataView.setItemsPerPage(10);
		add(projectDataView);
		projectDataView.setOutputMarkupPlaceholderTag(true);
		add(new PageNavigator("projectNavigator", projectDataView));

		// Add createActivityForm
		add(new CreateProjectForm("createProjectForm"));

		// Add updateActivityForm
		add(new UpdateProjectForm("updateProjectForm"));

		// Add DeleteActivityForm
		add(new DeleteProjectForm("deleteProjectForm"));
	}

	/**
	 * 
	 * @author ahmad
	 *
	 */
	private final class CreateProjectForm extends Form<ValueMap> {

		private static final long serialVersionUID = 1L;

		public CreateProjectForm(String formId) {

			super(formId, new CompoundPropertyModel<ValueMap>(new ValueMap()));
			setMarkupId("projectCreateForm");
			add(new TextField<String>("projectNameInput").setType(String.class));
			add(new TextField<String>("cbuildingNameInput").setType(String.class));
		}

		/**
		 * Show the resulting valid create
		 */
		@Override
		public final void onSubmit() {

			ValueMap values = getModelObject();

			// Step 1. Get the values
			Object projectName = values.get("projectNameInput");
			Object buildingName = values.get("cbuildingNameInput");

			// Step 2. Submit To Facade and Database
			if (projectName != null && buildingName != null) {

				StringValue orgId = getPageParameters().get("orgid");

				Project project = new Project();
				project.setProjectName(projectName.toString());
				project.setBuildingName(buildingName.toString());
				projFacade.createNewProject(project, orgId.toLongObject());
			}
			
			// Step 3. Refresh the page
			refresh();
		}
	}

	/**
	 * 
	 * @author ahmad
	 *
	 */
	private final class UpdateProjectForm extends Form<ValueMap> {

		private static final long serialVersionUID = 1L;

		public UpdateProjectForm(String formId) {

			super(formId, new CompoundPropertyModel<ValueMap>(new ValueMap()));
			setMarkupId("projectUpdateForm");
			add(new TextField<String>("uprojectNameInput").setType(String.class));
			add(new TextField<String>("uBuildingNameInput").setType(String.class));
			add(new HiddenField<String>("uprojectdIdHidden").setType(String.class));
		}

		/**
		 * Show the resulting valid edit
		 */
		@Override
		public final void onSubmit() {

			ValueMap values = getModelObject();

			// Step 1. Get the values
			Object projectName = values.get("uprojectNameInput");
			Object buildingName = values.get("uBuildingNameInput");
			Object projectId = values.get("uprojectdIdHidden");

			// Step 2. Submit To Facade and Database
			if (projectName != null && projectId != null) {
				Project project = new Project();
				project.setId(new Long(projectId.toString()));
				project.setProjectName(projectName.toString());
				project.setBuildingName(buildingName.toString());
				projFacade.updateProject(project);
			}
			
			// Step 3. Refresh the page
			refresh();
		}
	}

	/**
	 * 
	 * @author ahmad
	 *
	 */
	private final class DeleteProjectForm extends Form<ValueMap> {

		private static final long serialVersionUID = 1L;

		public DeleteProjectForm(String formId) {

			super(formId, new CompoundPropertyModel<ValueMap>(new ValueMap()));
			setMarkupId("deleteUpdateForm");
			add(new HiddenField<String>("dprojectIdHidden").setType(String.class));
		}

		/**
		 * Show the resulting valid edit
		 */
		@Override
		public final void onSubmit() {
			ValueMap values = getModelObject();
			projFacade.deleteProject(values.getAsLong("dprojectIdHidden"));
			
			// Step 3. Refresh the page
			refresh();
		}
	}
}
