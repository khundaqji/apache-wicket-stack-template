package org.software.khundaqji.project.tracker.activities;

import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.string.StringValue;
import org.apache.wicket.util.value.ValueMap;
import org.software.khundaqji.project.common.AbstractWebPage;
import org.software.khundaqji.project.common.navigation.PageNavigator;
import org.software.khundaqji.tracker.persistence.entity.ProjectActivity;

/**
 * 
 * @author Ahmad
 *
 */
@AuthorizeInstantiation("SIGNED_IN")
public class ProjectActivitiesPage extends AbstractWebPage {

	/**
	 * Generated Serial Version
	 */
	private static final long serialVersionUID = -6265568444709632670L;

	/**
	 * Creates a new OrganizationProjectsPage
	 * 
	 * @param parameters
	 */
	public ProjectActivitiesPage(final PageParameters parameters) {

		super(parameters);

		StringValue projectId = parameters.get("projectId");
		this.setupProjectTable(projectId.toString());
	}


	/**
	 * 
	 * @param org
	 */
	private void setupProjectTable(String projectId) {

		List<ProjectActivity> projects = projFacade.findProjectActivities(new Long(projectId));
		ListDataProvider<ProjectActivity> projectDataProvider = new ListDataProvider<ProjectActivity>(projects);
		
		DataView<ProjectActivity> projectDataView = new DataView<ProjectActivity>("activities", projectDataProvider) {

			private static final long serialVersionUID = 2318937182208288483L;

			@Override
			protected void populateItem(Item<ProjectActivity> item) {
				ProjectActivity activity = (ProjectActivity) item.getModelObject();
				item.add(new Label("acitivityId", activity.getId()));
				item.add(new Label("acitivityName", activity.getName()));

				final Label progressLabel = new Label("acitivityProgress", activity.getProgress());
				progressLabel.add(AttributeModifier.replace("style", "width:" + activity.getProgress() + "%"));
				item.add(progressLabel);

				final Button deleteButton = new Button("deleteBtn");
				deleteButton.add(AttributeModifier.replace("id", activity.getId()));
				item.add(deleteButton);

				final Button updateButton = new Button("updateBtn");
				updateButton.add(AttributeModifier.replace("id", activity.getId()));
				item.add(updateButton);
			}
		};
		projectDataView.setItemsPerPage(10);
		add(projectDataView);
		projectDataView.setOutputMarkupPlaceholderTag(true);
		add(new PageNavigator("activityNavigator", projectDataView));

		// Add createActivityForm
		add(new CreateActivityForm("createActivityForm"));

		// Add updateActivityForm
		add(new UpdateActivityForm("updateActivityForm"));

		// Add DeleteActivityForm
		add(new DeleteActivityForm("deleteActivityForm"));

	}

	/**
	 * 
	 * @author ahmad
	 *
	 */
	private final class CreateActivityForm extends Form<ValueMap> {

		private static final long serialVersionUID = 1L;

		public CreateActivityForm(String formId) {

			super(formId, new CompoundPropertyModel<ValueMap>(new ValueMap()));
			setMarkupId("activityCreateForm");
			add(new TextField<String>("activityNameInput").setType(String.class));

			NumberTextField<Integer> actProgressInput = new NumberTextField<Integer>("activityProgressInput");
			actProgressInput.setType(Integer.class);
			add(actProgressInput);
		}

		/**
		 * Show the resulting valid create
		 */
		@Override
		public final void onSubmit() {

			ValueMap values = getModelObject();

			// Step 1. Get the values
			Object activityName = values.get("activityNameInput");
			Object activityProgress = values.get("activityProgressInput");

			// Step 2. Submit To Facade and Database
			if (activityName != null) {

				StringValue projectId = getPageParameters().get("projectId");

				ProjectActivity activity = new ProjectActivity();
				activity.setName(activityName.toString());
				activity.setProgress(new Integer(activityProgress.toString()));
				actFacade.createNewActivity(activity, projectId.toLongObject());
			}

			// Step 3. Clear the fields
			refresh();
		}
	}

	/**
	 * 
	 * @author ahmad
	 *
	 */
	private final class UpdateActivityForm extends Form<ValueMap> {

		private static final long serialVersionUID = 1L;

		public UpdateActivityForm(String formId) {

			super(formId, new CompoundPropertyModel<ValueMap>(new ValueMap()));
			setMarkupId("activityUpdateForm");
			add(new TextField<String>("uactivityNameInput").setType(String.class));
			add(new NumberTextField<Integer>("uactivityProgressInput").setType(Integer.class));
			add(new HiddenField<String>("uactivityIdHidden").setType(String.class));
		}

		/**
		 * Show the resulting valid edit
		 */
		@Override
		public final void onSubmit() {

			ValueMap values = getModelObject();

			// Step 1. Get the values
			Object activityName = values.get("uactivityNameInput");
			Object activityProgress = values.get("uactivityProgressInput");
			Object activityId = values.get("uactivityIdHidden");

			// Step 2. Submit To Facade and Database
			if (activityName != null && activityProgress != null && activityId != null) {
				ProjectActivity activity = new ProjectActivity();
				activity.setId(new Long(activityId.toString()));
				activity.setName(activityName.toString());
				activity.setProgress(new Integer(activityProgress.toString()));
				actFacade.updateActivity(activity);
			}

			// Step 3. Clear the fields
			refresh();
		}
	}

	/**
	 * 
	 * @author ahmad
	 *
	 */
	private final class DeleteActivityForm extends Form<ValueMap> {

		private static final long serialVersionUID = 1L;

		public DeleteActivityForm(String formId) {

			super(formId, new CompoundPropertyModel<ValueMap>(new ValueMap()));
			setMarkupId("deleteUpdateForm");
			add(new HiddenField<String>("dactivityIdHidden").setType(String.class));
		}

		/**
		 * Show the resulting valid edit
		 */
		@Override
		public final void onSubmit() {
			ValueMap values = getModelObject();
			actFacade.deleteActivity(values.getAsLong("dactivityIdHidden"));
			refresh();
		}
	}
}