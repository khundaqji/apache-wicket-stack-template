package org.software.khundaqji.project.tracker;

import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.value.ValueMap;
import org.software.khundaqji.project.common.AbstractWebPage;
import org.software.khundaqji.project.common.navigation.PageNavigator;
import org.software.khundaqji.project.tracker.projects.OrganizationProjectsPage;
import org.software.khundaqji.tracker.persistence.entity.Organization;

/**
 * 
 * @author Ahmad
 * @version 1.1
 * @since 1.0
 *
 */
@AuthorizeInstantiation("SIGNED_IN")
public class TrackerPage extends AbstractWebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3165186528859293241L;

	private static final String ORG_LABEL_KEY = "orgs";

	public TrackerPage(final PageParameters parameters) {

		super(parameters);

		List<Organization> orgList = orgFacade.findOrgainzations();
		this.populateOrganizations(orgList);

	}

	/**
	 * Populating the Organization data
	 *
	 * @param orgs
	 */
	private void populateOrganizations(List<Organization> orgs) {

		DataView<Organization> orgDataView = new DataView<Organization>(ORG_LABEL_KEY,
				new ListDataProvider<Organization>(orgs)) {

			private static final long serialVersionUID = -8696763166231405490L;

			public void populateItem(final Item<Organization> item) {

				final Organization org = (Organization) item.getModelObject();

				item.add(new Label("id", org.getId()));
				item.add(new Label("name", org.getName()));
				item.add(new Label("description", org.getDescription()));

				item.add(new Link<Void>("totalProject") {

					private static final long serialVersionUID = -2839400426374822427L;

					@Override
					public void onClick() {
						PageParameters params = new PageParameters();
						params.add("orgid", org.getId());
						setResponsePage(OrganizationProjectsPage.class, params);
					}
				});

				item.add(new Label("runningProjects", org.getProjects().size()));

				final Button deleteButton = new Button("deleteBtn");
				deleteButton.add(AttributeModifier.replace("id", org.getId()));
				item.add(deleteButton);

				final Button updateButton = new Button("updateBtn");
				updateButton.add(AttributeModifier.replace("id", org.getId()));
				item.add(updateButton);
			}
		};

		orgDataView.setItemsPerPage(10);
		add(orgDataView);
		add(new PageNavigator("navigator", orgDataView));

		// Add createActivityForm
		add(new CreateOrgForm("createOrgForm"));

		// Add updateActivityForm
		add(new UpdateOrgForm("updateOrgForm"));

		// Add DeleteActivityForm
		add(new DeleteOrgForm("deleteOrgForm"));
	}

	/**
	 * 
	 * Creational form used for the organization
	 * 
	 * @author ahmad
	 *
	 */
	private final class CreateOrgForm extends Form<ValueMap> {

		/**
		 * Generated Serial Version
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Organization Bean to be binded with this form
		 */
		private Organization organization = new Organization();

		/**
		 * Class constructor to create a new form
		 * 
		 * @param formId
		 */
		public CreateOrgForm(String formId) {

			super(formId, new CompoundPropertyModel<ValueMap>(new ValueMap()));
			setMarkupId("orgUpdateForm");

			// Step 1. Initializing the form components
			add(new TextField<String>("orgNameInput", new PropertyModel<String>(organization, "name"))
					.setType(String.class));
			add(new TextField<String>("corgDescInput", new PropertyModel<String>(organization, "description"))
					.setType(String.class));
		}

		/**
		 * Show the resulting valid create
		 */
		@Override
		public final void onSubmit() {

			// Step 2. Submit To Facade and Database
			if (organization.getName() != null && organization.getDescription() != null) {
				Organization org = new Organization();
				org.setName(organization.getName());
				org.setDescription(organization.getDescription());
				orgFacade.createOrganization(org);
			}

			// Step 3. Refresh the page
			refresh();
		}
	}

	/**
	 * 
	 * @author ahmad
	 *
	 */
	private final class UpdateOrgForm extends Form<ValueMap> {

		/**
		 * Generated Serial version
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Organization Bean to be binded with this form
		 */
		private Organization organization = new Organization();

		public UpdateOrgForm(String formId) {

			super(formId, new CompoundPropertyModel<ValueMap>(new ValueMap()));
			setMarkupId("updateOrgForm");

			// Step 1. Initializing the form components
			add(new TextField<String>("uorgNameInput", new PropertyModel<String>(organization, "name"))
					.setType(String.class));
			add(new TextField<String>("uOrgDescInput", new PropertyModel<String>(organization, "description"))
					.setType(String.class));
			add(new HiddenField<Long>("uorgdIdHidden", new PropertyModel<Long>(organization, "id"))
					.setType(Long.class));
		}

		/**
		 * Show the resulting valid edit
		 */
		@Override
		public final void onSubmit() {

			// Step 2. Submit To Facade and Database
			if (organization.getName() != null && organization.getDescription() != null
					&& organization.getId() != null) {
				Organization org = new Organization();
				org.setName(organization.getName());
				org.setDescription(organization.getDescription());
				org.setId(organization.getId());
				orgFacade.updateOrganization(org);
			}

			// Step 3. Refresh the page
			refresh();
		}
	}

	/**
	 * 
	 * @author ahmad
	 *
	 */
	private final class DeleteOrgForm extends Form<ValueMap> {

		private static final long serialVersionUID = 1L;

		public DeleteOrgForm(String formId) {

			super(formId, new CompoundPropertyModel<ValueMap>(new ValueMap()));
			setMarkupId("deleteUpdateForm");
			add(new HiddenField<String>("dorgIdHidden").setType(String.class));
		}

		/**
		 * Show the resulting valid edit
		 */
		@Override
		public final void onSubmit() {
			ValueMap values = getModelObject();
			orgFacade.deleteOrganization(values.getAsLong("dorgIdHidden"));

			// Step 3. Refresh the page
			refresh();
		}
	}
}
