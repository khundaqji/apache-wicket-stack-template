package org.software.khundaqji.project.common;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import org.software.khundaqji.project.HomePage;
import org.software.khundaqji.project.domain.business.OrganizationFacade;
import org.software.khundaqji.project.domain.business.ProjectActivityFacade;
import org.software.khundaqji.project.domain.business.ProjectsFacade;
import org.software.khundaqji.project.login.LoginPage;
import org.software.khundaqji.project.tracker.TrackerPage;

/**
 * A Common webpage that might have the navigation infomration
 * 
 * @author Ahmad
 * @version 1.1
 * @since 1.0
 *
 */
public abstract class AbstractWebPage extends WebPage {

	/**
	 * Generated Seial Version of this class
	 */
	private static final long serialVersionUID = 8079913825071922686L;

	protected static final String PAGE_TITLE = "Golden Real Estate";
	protected static final String HOME_PAGE_LINK = "homePageLink";
	protected static final String LOGIN_PAGE_LINK = "loginPageLink";
	protected static final String ABOUT_PAGE_LINK = "aboutPageLink";
	protected static final String TRACKING_PAGE = "trackingPageLink";
	private static final String LOGOUT_PAGE_LINK = "logOut";

	protected OrganizationFacade orgFacade = new OrganizationFacade();

	protected ProjectsFacade projFacade = new ProjectsFacade();

	protected ProjectActivityFacade actFacade = new ProjectActivityFacade();

	/**
	 * Creates a new Page with the specified parameters
	 * 
	 * @param parameters
	 */
	public AbstractWebPage(final PageParameters parameters) {
		// Step 1. Calling the super class
		super(parameters);
	}

	/**
	 * Used to refresh the page after some modifications actions
	 */
	protected void refresh() {
		setResponsePage(getPageClass(), getPageParameters());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.wicket.Component#onConfigure()
	 */
	@Override
	protected void onConfigure() {
		super.onConfigure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.wicket.Page#onInitialize()
	 */
	@Override
	protected void onInitialize() {

		super.onInitialize();

		add(new Label("pageTitle", PAGE_TITLE));

		// Step 2. Adding common links to the page
		add(new Link<Void>(LOGIN_PAGE_LINK) {

			private static final long serialVersionUID = 5790527659254159478L;

			@Override
			public void onClick() {
				setResponsePage(LoginPage.class);
			}
		});

		add(new Link<Void>(ABOUT_PAGE_LINK) {

			private static final long serialVersionUID = 5790527659254159478L;

			@Override
			public void onClick() {
				setResponsePage(HomePage.class);
			}
		});

		add(new Link<Void>(TRACKING_PAGE) {

			private static final long serialVersionUID = 8377048737673789284L;

			@Override
			public void onClick() {
				setResponsePage(TrackerPage.class);
			}

		});

		/**
		 * Home page link on all pages
		 */
		add(new Link<Void>(HOME_PAGE_LINK) {

			private static final long serialVersionUID = -7757851308266946864L;

			@Override
			public void onClick() {
				setResponsePage(getApplication().getHomePage());
			}
		});

		/**
		 * sign out link to be on all pages
		 */
		add(new Link<Void>(LOGOUT_PAGE_LINK) {

			private static final long serialVersionUID = -4228312458686467798L;

			@Override
			public void onClick() {
				AuthenticatedWebSession.get().invalidate();
				setResponsePage(getApplication().getHomePage());
			}
		});
	}
}
