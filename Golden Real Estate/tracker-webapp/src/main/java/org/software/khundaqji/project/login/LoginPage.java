package org.software.khundaqji.project.login;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.string.Strings;
import org.software.khundaqji.project.common.AbstractWebPage;

/**
 * This Wicket page represents the Centrelized login page, where users need to
 * use in order to go further into the system
 * 
 * @author Ahmad
 * @version 1.0
 * @since 1.0
 */
public class LoginPage extends AbstractWebPage {

	/**
	 * Class Generated Serial Version
	 */
	private static final long serialVersionUID = 7620256621682317700L;

	/**
	 * Creates a new LoginPage
	 * 
	 * @param parameters
	 */
	public LoginPage(final PageParameters parameters) {
		super(parameters);
	}

	private String username;
	private String password;

	@Override
	protected void onInitialize() {

		super.onInitialize();

		StatelessForm<LoginPage> form = new StatelessForm<LoginPage>("form") {

			private static final long serialVersionUID = -7910795241003928370L;

			@Override
			protected void onSubmit() {
				if (Strings.isEmpty(username))
					return;

				boolean authResult = AuthenticatedWebSession.get().signIn(username, password);
				/*
				 * if authentication succeeds redirect user to the requested an
				 * admin page
				 */
				if (authResult) {
					continueToOriginalDestination();
				}
			}
		};

		form.setDefaultModel(new CompoundPropertyModel<LoginPage>(this));

		form.add(new TextField<LoginPage>("username"));
		form.add(new PasswordTextField("password"));

		add(form);
	}
}
