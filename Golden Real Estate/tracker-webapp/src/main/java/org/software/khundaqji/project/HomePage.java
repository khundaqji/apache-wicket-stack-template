package org.software.khundaqji.project;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.software.khundaqji.project.common.AbstractWebPage;

/**
 * 
 * @author Ahmad
 *
 */
public class HomePage extends AbstractWebPage {
	
	/**
	 * Serial Version of this Page
	 */
	private static final long serialVersionUID = 1L;
	
	public HomePage(final PageParameters parameters) {
		
		super(parameters);		
	}
}
